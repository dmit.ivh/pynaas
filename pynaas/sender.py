from functools import wraps
import json
import requests

DEFAULT_URL = "http://localhost:8000"
DEFAULT_SENDER_ID = 0


def prepare_json(sender_id: int, message: str) -> str:
    data = {
        "ID": str(sender_id),
        "Message": message
    }
    return json.dumps(data)


def _make_message(sender_id: int, message: str) -> str:
    return f"From:\t{sender_id}\nMessage:\n{str(message)}"


def send_to(sender_id: int, message: str, url: str) -> int:
    payload = prepare_json(sender_id, message)
    r = requests.get(url, data=payload)
    return r.status_code


def send(sender_id: int, url: str, message: str):
    message = _make_message(sender_id, message)
    send_to(sender_id, message, url)


def notification_after(*, sender_id: int = None, url: str = None):
    global DEFAULT_URL, DEFAULT_SENDER_ID
    if url is None:
        url = DEFAULT_URL
    if sender_id is None:
        sender_id = DEFAULT_SENDER_ID

    def _process_wrapper(function):
        nonlocal sender_id, url

        @wraps(function)
        def _wrapper(*args, **kwargs):
            result = function(*args, **kwargs)
            message = _make_message(sender_id, result)
            send_to(sender_id, message, url)
            return result

        return _wrapper

    return _process_wrapper


@notification_after()
def notification_after_perform(function, *args, **kwargs):
    return function(*args, **kwargs)


def from_sender_notification(sender_id: int, function, *args, **kwargs):
    return notification_after(sender_id=sender_id)(function)(*args, **kwargs)


def from_sender_notification_to(sender_id: int, url: str, function, *args, **kwargs):
    return notification_after(sender_id=sender_id, url=url)(function)(*args, **kwargs)
