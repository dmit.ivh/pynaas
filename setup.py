#!/usr/bin/env python

from distutils.core import setup

setup(
    name="pynass",
    version="0.1",
    description="Wrappers of functions for work with gonaas",
    author="Dmitri Ivakhnenko",
    author_email="dmit.ivh@gmail.com",
    url="https://gitlab.com/dmit.ivh/pynaas",
    packages=["pynaas"],
    license="GPLv3"
)
